double my_pow(double val, unsigned int st) {
	int res = 1;
	for (int i = 0; i < st; i++)
		res *= val;

	return res;
}