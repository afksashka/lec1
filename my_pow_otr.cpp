double my_pow(double val, int st) {
	double res = 1;

	if (st > 0) {
		for (int i = 0; i < st; i++)
			res *= val;
		return res;
	}
	if (st == 0)
		return 1;
	if (st < 0)
	{
		st = st * (-1);
		for (int i = 0; i < st; i++)
			res *= val;
		res = 1 / res;
		return res;
	}

}